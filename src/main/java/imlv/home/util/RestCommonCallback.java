package imlv.home.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.*;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.util.Base64;
import java.util.logging.Level;

public class RestCommonCallback {

    private URL url = null;
    private HttpURLConnection httpCon = null;

    public RestCommonCallback() throws Exception {
        url = new URL("http://localhost:8090/rest/api/space");
        httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setUseCaches(false);
        httpCon.setDoInput(true);
        httpCon.setDoOutput(true);
        String credentials = "admin" + ":" + "admin";
        String encoding = Base64.getEncoder().encodeToString(credentials.getBytes());
        
        httpCon.setRequestMethod("POST");
        httpCon.setRequestProperty("Content-Type", "application/json");
        httpCon.setRequestProperty("Authorization", String.format("Basic %s", encoding));
        httpCon.setRequestProperty("charset", "utf-8");
        httpCon.setRequestProperty("Accept", "application/json");
    }

    public void sendRequest(String spaceKey, String spaceName) {
        try {
            OutputStream os = this.getHttpCon().getOutputStream();
//            OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
            BufferedWriter osw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            String contentJson = getContentJsonRestAPICallback();
            contentJson = mergeInput(contentJson, spaceKey, spaceName);
            contentJson = replaceJsonContent(contentJson);

            osw.write(contentJson);
            osw.flush();
            osw.close();
            this.getHttpCon().connect();
        } catch (Exception e) {
            java.util.logging.Logger.getLogger(RestCommonCallback.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public String getResponse() throws Exception {
        String result = "";
        BufferedInputStream bis = null;
        ByteArrayOutputStream buf = null;
        try {
            bis = new BufferedInputStream(getInputStream());
            buf = new ByteArrayOutputStream();
            int result2 = bis.read();
            while (result2 != -1) {
                buf.write((byte) result2);
                result2 = bis.read();
            }
            result = buf.toString();
        } catch (Exception e) {
            java.util.logging.Logger.getLogger(RestCommonCallback.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            bis.close();
            buf.close();
        }
        return result;
    }

    public String getContentJsonRestAPICallback() throws Exception {
        InputStream contentJson = this.getClass().getResourceAsStream(Constants.PATH_JSON_FILE_INFO_NEW_SPACE);
        Reader reader = new InputStreamReader(contentJson, "UTF-8");
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = (JsonObject) jsonParser.parse(reader);
        String contentJsonInfoSpace = jsonObject.toString();
        contentJson.close();
        reader.close();
        return contentJsonInfoSpace;
    }

    public String replaceJsonContent(String contentJson) {
        contentJson = contentJson.replace("\t", "\\t");
        contentJson = contentJson.replace("\r\n", "\\r\\n");
        contentJson = contentJson.replace("\r", "\\r");
        contentJson = contentJson.replace("\n", "\\n");
        return contentJson;
    }

    public String mergeInput(String contentJson, String spaceKey, String spaceName) {
        contentJson = contentJson.replace("\"%SPACE_KEY%\"", "\"" + spaceKey + "\"");
        contentJson = contentJson.replace("\"%SPACE_NAME%\"", "\"" + spaceName + "\"");
        return contentJson;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public HttpURLConnection getHttpCon() {
        return httpCon;
    }

    public void setHttpCon(HttpURLConnection httpCon) {
        this.httpCon = httpCon;
    }

    public OutputStream getOutputStream() throws IOException {
        return this.getHttpCon().getOutputStream();
    }

    public InputStream getInputStream() throws IOException {
        return this.getHttpCon().getInputStream();
    }
}
