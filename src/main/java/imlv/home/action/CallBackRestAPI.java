package imlv.home.action;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAction;
import static com.opensymphony.xwork.Action.SUCCESS;
import com.opensymphony.xwork.ActionContext;
import imlv.home.action.biz.DocumentKanriBiz;
import imlv.home.util.ActionContextSupport;
import imlv.home.util.Constants;
import imlv.home.util.RestCommonCallback;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class CallBackRestAPI extends AbstractSpaceAction implements Beanable{

    Map<String, Object> map = new HashMap<String,Object>();
    
    @Override
    public Object getBean() {
        return map;
    }
    
    public String createSpaceRest() {
        try {
             ActionContext context = ActionContext.getContext();
            String spaceKey = ActionContextSupport.getParameterAsString(context, "spaceKey");
            String spaceName = ActionContextSupport.getParameterAsString(context, "spaceName");

            RestCommonCallback restCommonCallback = new RestCommonCallback();
            restCommonCallback.sendRequest(spaceKey, spaceName);
            String result = restCommonCallback.getResponse();
            return SUCCESS;
        } catch (Exception e) {
            java.util.logging.Logger.getLogger(CallBackRestAPI.class.getName()).log(Level.SEVERE, null, e);
        }

        return SUCCESS;
    }
}
