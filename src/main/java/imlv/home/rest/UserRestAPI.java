package imlv.home.rest;

import com.atlassian.confluence.user.ConfluenceUser;
import imlv.home.factory.ManualVersionFactory;
import imlv.home.model.User;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class UserRestAPI {
    @GET
    @Path("users")
    public Response getUncompletedUsers() {
        return Response.ok(new User("John","Smith")).build();
    }
    
    @GET
    @Path("admins")
    public Response getAdminsInConfluence(@QueryParam("key") String keySearch) {
        Map<String, Object> map = new HashMap<>();
//        final ConfluenceUser user = ManualVersionFactory.getUserAccessor().getUserByName(keySearch);
        return Response.ok(map).build();
    }
}
